"use client"
import { LeftMenu } from '@/components/Acordion';
import { Button } from '@nextui-org/button';
import { Card, CardHeader, CardBody, CardFooter, Divider, Link, Image, Accordion } from "@nextui-org/react";
import { Code } from "@nextui-org/react";
import { CustomButton } from '@/components/ButtonConfeti'

function Main() {
    return (
        <>
            <main className='flex'>
                <aside>
                    <LeftMenu />
                </aside>
                <div>
                    <CustomButton />
                    <Button>Click me</Button>
                    <Card className="max-w-[400px]">
                        <CardHeader className="flex gap-3">
                            <Image
                                alt="nextui logo"
                                height={40}
                                radius="sm"
                                src="https://avatars.githubusercontent.com/u/86160567?s=200&v=4"
                                width={40}
                            />
                            <div className="flex flex-col">
                                <p className="text-md">NextUI</p>
                                <p className="text-small text-default-500">nextui.org</p>
                            </div>
                        </CardHeader>
                        <Divider />
                        <CardBody>
                            <p>Make beautiful websites regardless of your design experience.</p>
                        </CardBody>
                        <Divider />
                        <CardFooter>
                            <Link
                                isExternal
                                showAnchorIcon
                                href="https://github.com/nextui-org/nextui"
                            >
                                Visit source code on GitHub.
                            </Link>
                        </CardFooter>
                    </Card>

                    <div className="flex flex-wrap gap-4">
                        <Code color="default">npm install @nextui-org/react</Code>
                        <Code color="primary">npm install @nextui-org/react</Code>
                        <Code color="secondary">npm install @nextui-org/react</Code>
                        <Code color="success">npm install @nextui-org/react</Code>
                        <Code color="warning">npm install @nextui-org/react</Code>
                        <Code color="danger">npm install @nextui-org/react</Code>
                    </div>
                </div>
            </main>
        </>
    )
}

export default Main