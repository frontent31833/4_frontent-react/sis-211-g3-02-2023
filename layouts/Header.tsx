import { NavbarComponent } from '@/components/NavBar'

function Header() {
    return (
        <header>
            <NavbarComponent />
        </header>
    )
}

export default Header