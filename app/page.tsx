/* eslint-disable @next/next/no-img-element */
"use client"

import Footer from "@/layouts/Footer"
import Header from "@/layouts/Header"
import Main from "@/layouts/Main"


function Home() {
  return (
    <>
      <Header />
      <Main />
      <Footer />
    </>
  )
}

export default Home